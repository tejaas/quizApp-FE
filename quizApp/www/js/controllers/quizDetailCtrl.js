angular.module('starter')

.controller('quizDetailCtrl', quizDetailCtrl);
function quizDetailCtrl($stateParams,currentQuiz,topics,papers,$state,$localForage,saveParams){
  var detail = this;
  detail.history =[];
    console.log($stateParams.topic);
    console.log($stateParams.paper);
    if($stateParams.topic!=null)
    {
        detail.type='topic';
        currentQuiz.type='topic';

        currentQuiz.paper=$stateParams.topic;
        saveParams.params.filterby=$stateParams.topic;
    }
    if($stateParams.paper!=null) {


        detail.type='paper';
        currentQuiz.type='paper';

        currentQuiz.paper = $stateParams.paper;
        saveParams.params.filterby= $stateParams.paper;
    }



detail.topicName=null;

if(currentQuiz.type=="topic"){
    detail.topic=topics;
_.each(detail.topic,function(t){
    if(currentQuiz.paper==t.id){
        detail.topicName= t.category;
        detail.imageLink= t.img;

    }

})

}
    else{
    detail.topic=papers;
    _.each(detail.topic,function(t){
        if(currentQuiz.paper==t.id)
            detail.topicName= t.category;
    })

}





  //TODO
  //get id of selected paper or topic from previous screen by state params
    currentQuiz.selectQuestions();
  //TODO
  //Fetch all questions from factory which have above id included. Push in an array and calculate length and duration (length x 1 min).
  //Display on the screen - Paper/Topic name, total questions, duration
    detail.questionslength=currentQuiz.filteredQuestions.length;
    detail.MinAttampquestions=Math.ceil((detail.questionslength*30)/100);


  //TODO
  //Start the quiz, navigate to question.html and start from id 0

  detail.goBack = function(){
    $state.go('home');
  }
//quizHistory show
//    detail.history=quizHistory.history;

    //get history from localForage

    var historyKey="history";
    $localForage.getItem(historyKey).then(function (data) {
        console.log("quiz history", data);
        if (data) {
            detail.history= data;
            console.log(detail.history);
            }
    });

    //filter history by type and paperId
    detail.matchTypePaperId=function(index){
        if(!(currentQuiz.type.localeCompare(detail.history[index].type))&&(currentQuiz.paper==detail.history[index].paper)){

            return true;
        }
        else{
            return false;
        }
    }
}

